# C-Programming

## CZ: Cvičení k výuce základů programování v C

### Popis
Tento projekt obsahuje sadu cvičení určených k výuce základů programování v jazyce C. 

### Struktura projektu


### Jak začít
1. Naklonujte repozitář:
    ```
    Spuste příkazovou řádku, vyberte kam chcete ulozit repozitář.
    Na webu stiskněte clone -> vyberte SSH key/HTTP URL.
    Nasledně do příkazové řádky:
    ```
    ```bash
        git clone <váš zkopirovaný SSH klíč/HTTP URL>
    ```
   
2. Editor(Doporučeno)
   - VSCode
   - CLion
   - Dá se psát kód i bez editoru, ale editor umožňuje nějakých syntaktických chyb
   
### Důležité odkazy
   Základní knihovny v C/C++: https://en.cppreference.com/w/

---

---

## EN: Exercises for Learning the Basics of C Programming

### Description
This project contains a set of exercises designed to teach the basics of programming in the C language.

### Project Structure
- **01_hello_world:** Základní program vypisující "Hello, World!" na obrazovku.


### Getting Started
1. Clone the repository:
    ```
    Open the command line, choose where you want to save the repository.
    On the website, click clone -> select SSH key/HTTP URL.
    Then in the command line:
    ```
    ```bash
        git clone <your copied SSH key/HTTP URL>
    ```

2. Editor (Recommended)
   - VSCode
   - CLion
   - It's possible to write code without an editor, but an editor helps avoid some syntax errors

### Important Links
Standard Libraries in C/C++: https://en.cppreference.com/w/
