The task is to implement a program that will calculate the balance in the bank account. The program will process transactions (deposit, withdrawal) and will take into account the interest rate.

We assume that the bank maintains accounts in crowns. It allows its clients to deposit and withdraw money, but always allows no more than one transaction per day. Furthermore, the bank is benevolent and allows unlimited debits. A positive balance bears interest at the credit rate, while a negative balance bears interest at the debit rate (which may vary). Interest is always charged on a daily basis. The amount of the balance and the amount of interest credited/held must always be expressed and calculated in crowns and whole cents. All amounts are rounded to zero (i.e. interest credited/retained of 1.3499 will be treated as 1.34). On a given day, the amount that was in the account at midnight will earn interest, so the deposit/withdrawal will not be reflected in interest until the following day.

The program input is the credit and debit interest rate values in the form of two decimal numbers. The values represent the daily interest in percentage. The following is a list of transactions. Each transaction consists of two numbers: the date of the transaction and the amount. The transactions are entered in order, i.e. the day numbers are incremented. A positive amount represents a deposit, a negative amount represents a withdrawal. A zero amount means the account is cancelled, the program responds by listing the total balance and terminating.

The output of the program is the value of the balance when the account is cancelled. The format of the output is clear from the examples below. Don't forget the line breaks after the statement.

If the input is not valid (there are non-numeric or meaningless values on the input), the program will detect this situation and print an error message. The format of the error message is again shown in the examples below. It is considered an error if the input:

- A non-numeric value,
- the order of the transactions is incorrect (the day numbers in the transaction do not form an increasing sequence); or
- a missing separator (amount) in the transaction entry.

If the program detects an error, it stops asking for further input values, prints an error message, and exits. Therefore, you must detect the error immediately after the value is loaded (do not delay checking the input data until after the entire input has been loaded). Write the error message to the standard output (do not write it to the standard error output).

### Sample of the program work:

#### First run:
Enter credit interest [%]:
0.01  
Enter debit interest [%]:
0.1  
Enter transactions:
0, 20000  
45, -5000  
100, 10000  
120, 0  
Balance: 25223.27

#### Second run:
Enter the credit interest [%]:
0.02  
Enter debit interest [%]:
0.4  
Enter transactions:
10, 20000  
25, -15000  
45, -10000  
70, 5000  
100, 0  
Balance: -491.27

#### Error situation:
Enter credit interest [%]:
abcd  
Incorrect input.

Enter credit interest [%]:
0.02  
Enter debit rate [%]:
0.3  
Enter transactions:
5, 2000  
0, 3000  
Incorrect input.