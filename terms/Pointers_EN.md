# Pointers

## 1. Basic use

Declaring a variable of type pointer:

```c
int * pi;
char * pc;
```

The variable `pi` is a pointer to `int`, `pc` is a pointer to `char`. The type `int` or `char` is a **domain pointer type**.
A pointer can be assigned the address of a variable:

```c
int * pi, i;
char c;
char * pc = &c;
pi = &i;
```

The unary prefix operator `&` denotes a **reference** operation. If we want to use the variable pointed to by the pointer,
we express it using the unary operator **dereference**. Thus, the operator `*`

| C code | Explanation of operation |
|--------------|--------------------------------------------------------------------------------------|
| `char c;` | Declaration of a variable `c` of type `char`. |
| `char * pc;` | Declaration of the variable `pc`, its type is a pointer to `char`. |
| `pc = &c;` | Binding the variable `pc` to `c`. The `pc` is now the address of the `c` variable in program memory. |
| `*pc = 'A';` | The value corresponding to the character `'A'` is stored at the address of `pc`. |

---

### NULL

The value `0` serves as an "empty, invalid" pointer that does not point to any variable in the C language. Symbolically
The symbolic name of this pointer is `NULL`. Dereferencing an empty pointer causes an error when the program runs:

```c
int main(void) {
    int * p = NULL;
    *p = 10; /* attempt to dereference an invalid pointer */
    return 0;
}
```

---

### Void*

If we need to create a pointer without a domain type (for example, because we don't know at design time what it will end up being)
we use the `void *` type. We can assign a value to this pointer, but we cannot dereference it, which is controlled by
at the compile stage of the program.

```c
int main(void) {
    int i;
    void * pi = &i;
    *pi = 10; /* this cannot be done */
    return 0;
}
```

---

### Pointer arithmetic

Pointers represent addresses to program memory, but not all arithmetic operations are allowed for them. Suppose,
we have two pointers whose **common domain type is T** and a number of type `int`.

| Operation | Result type | Description |
|------------|--------------|------------------------------------------------------------------------------------|
| `T* + int` | `T*` | Adding `n` to a pointer of type `T*` means shifting it by n times the length of type `T`. |
| `T* - int` | `T*` | Subtracting `n` from a pointer of type `T*` means shifting it by n times the length of type `T`. |
| | | The prefix and postfix operators `++ and --` work similarly. | |
| `T* - T*` | `int` | The number of elements of type `T` that can be stored between the addresses of the specified operands. |
| `T* < T*` | `int` | Determine whether the first pointer points in memory before the second. |
| | | Similarly for other relational operators `>`, `<=`, `>=`, `==`, `!=`. |
| | | Here the same domain type of pointers is not required. |

### Using the wrong operator

The C language does not keep track of what type of information is present at each memory location, i.e., if
we access memory, we as programmers need to know what type of data is in that location. To program this information
is conveyed to the program by the domain type of pointer we use. Using an incorrect pointer leads to a poorly discoverable
error:

```c
int main (void) {
    int * pi; /* !!!!! pointer to int!!!! */
    char c[] = {'A','h','o','j', ' ', 'F', 'I', 'T', 'e', '\0'};
    printf("content of c: %s\n",c); /* string check */
    pi = &c[5]; /* set the pointer to the sixth character, F */
    printf("address in c: %u\n", c);
    printf("address in c[5]: %u\n", &c[5]);
    printf("address in pi: %u\n", pi);
    printf("contents of c: %s\n", c);
    *pi = 'B'; /* rewrite F to B?????????????? */
    printf("contents of c: %s\n", c);
    return 0;
}
```

The output of the program can look like this (the result depends on endianness, current memory usage, ..):

```c
c content: Hello FITe
address in c: 2293554
address c[5]: 2293559
address in pi: 2293559
contents of c: Hi FITe
contents of c: Hi B
```

You can see from the preceding code that when using a pointer to an int (most often 4 bytes), it works with not only one byte, where the
where the `F` character is located, but also with 3 neighbors

---

## 2. Array pointer

An array of type `T` is essentially a constant pointer to T: suppose `int a[10], *pa, i;`, then

| Expression | Equivalent expression | Comment |
|-----------------------------------------|--------------------------------------|-------------------------------------------------------|
| `pa = a` | `pa = &a[0]` | The array is actually the address of the first element. |
| `*(a + 2) = 3` | `a[2] = 3` | Sets the third element to 3. |
| `*(2 + a) = 3` | `2[a] = 3` | Set the third element to 3, not nice to be functional. |
| `*(a + i) = 4` | `a[i] = 4` | Generalization. |
| `for (pa = a; pa < a + 10; *pa++ = 0);` | `for (i = 0; i < 10; i++) a[i] = 0;` | Arrays can be traversed with pointers. |
| `int sectiPrvky(int array[])` | `int sectiPrvky(int * array)` | Array as a function parameter. |

### Combination of * and ++

| Expression | Meaning |
|----------|------------------------------------------------------------------------------------------|
| `*p++` | Access the data referenced by pointer p and then increment p (move it to the next element) |
| `(*p)++` | Access the data referenced by pointer p and then increment the data. |
| `*++p` | First increment p and then access the data referenced by p. |
| | `++*p` | First increment the data referenced by p and then use that data. |

### Using const

If we declare an array, then the variable that represents it must not change (checked at the compile level). Thus
cannot:

```c
int a[10];
a++;
```

| Function declaration | Meaning |
|--------------------------------|--------------------------------------------------------------------------------------------|
| `int xyz(const int * uk);` | The data pointed to by **uk** must not be changed, but **uk** can be redirected to other data |
| `int xyz(int * const uk);` | The **uk** pointer must not be redirected, but the data referenced by it can be changed. |
| `int xyz(const int * const uk` | Neither **uk** nor the data may be changed. |

### Assignment between arrays

No assignment is defined for an array. Thus, the idea that one can simply transfer data from another array into an array type variable is
odd:

```c
int a[2], b[] = {1,2};
a = b;
```

This cannot be done. If we need to transfer data from one array to another, we need to do it by cycling element by element or
we can use functions that can work with larger sections of memory to speed up the copying. One of these functions is
function [memcpy](https://en.cppreference.com/w/c/string/byte/memcpy):

```c
void * memcpy(void * dest, const void * src, size_t count);
```

On the other hand, the assignment between pointers is defined, again note that the referenced data is not copied, only
the pointer is redirected:

```c
int main(void) {
    int * p = (int *)malloc(2*sizeof(int)); // Dynamically allocate an array of two elements.
    int * q = (int *)malloc(2*sizeof(int)); // And one more.
    p[0] = 1; // Set the value of the first element of the array p to 1.
    q[0] = 22; // Similarly for the first element of the second array.
    p = q; // Redirect the pointer, do not copy the values and the block of memory allocated for p
           // becomes unreachable and is released by the operating system when the program finishes.
    printf("%d\n", p[0]); // Prints 22 because p points to the same location as q.
    q[0] = 42; // Sets the first element of the q array.
    printf("%d\n", p[0]); // Prints 42, both pointers refer to the same value!
    free(p); // Frees the memory allocated for q.
    free(q); // The memory for array q has already been freed, if the operating system has already freed it in the meantime.
             // has allocated it to another variable, it probably won't end well sooner or later.
    return 0; // We didn't free the memory and took a risk with free, yet we return success.
}
```
