# Klíčová slova

## 2. Klíčová slova

V jazyce C je 32 klíčových slov:

| auto   | break  | case     | char   | const    | continue | default  | do    |
|--------|--------|----------|--------|----------|-----------|---------|-------|
| double | else   | enum     | extern | float    | for      | goto     | if    |
| int    | long   | register | return | short    | signed   | sizeof   | static|
| struct | switch | typedef  | union  | unsigned | void     | volatile | while |

---
### auto
Toto klíčové slovo definuje, že následující proměnná má lokální platnost. Zápis:
```c
auto int au;
```
Protože je pro lokální proměnné lokální platnost přednostní, je klíčové slovo auto používáno velice zřídka.

Klíčové slovo auto má jiný význam od standardu C++11, kde označuje proměnou, jejíž typ je odvozen pomocí typové inference. Protože auto je implicitní paměťová třída pro lokální proměnné, není dobrý nápad klíčové slovo uvádět

----
### break

Ukončuje vykonávání příkazu `while`, `do while`, `for` a `switch`. Program poté pokračuje za těmito příkazy. V případě vnoření více těchto příkazů, ukončuje ten nejvnitřnější.

---
### case

Součást příkazu `switch`

---
### char
Datový typ pro uložení znaků

---

### const
Klíčové slovo `const` je kvalifikátor určující, že následující proměnnou či ukazatel použitý jako parametr nelze změnit. Použití u proměnných `const int max = 20;` definuje konstantní proměnnou `max` typu `int` a nastaví ji na hodnotu `20`. Při pokusu o její změnu, například `max = 45;` způsobí chybu při překladu.

> Proměnná deklarovaná s kvalifikátorem `const` může být změněna nepřímo pomocí ukazatele `(int)&max = 35;`

> Podobný význam má použití příkazu define `#define MAX 20`  
Zatímco použití const alokuje místo pro uložení konstantní proměnné, define určuje preprocesoru, aby všechny výskyty identifikátoru `MAX` ve zdrojovém kódu nahradil řetězcem `20`. Nic se nemusí alokovat.

---

### continue
Ukončuje vykonávání právě probíhajícího cyklu příkazu `while`, `do while` a `for`. V případě vnoření více těchto příkazů, ukončuje ten nejvnitřnější.  
To znamená, když se běh kódu dostane k `continue`, tak se přeruší kód a pokračuje na další iteraci.
  
Kód s `break` vypíše pouze `1`
```c
for (int i = 1; i < 10; i++) {
	if (i % 2 == 0)
		break;
	printf("%d\n", i);
}
```  
Následující kód s `continue` vypíše pouze čísla `1 3 5 7 9`

```c
for (int i = 1; i < 10; i++) {
    if (i % 2 == 0)
    	continue;
    // Pokud je sudé tak se tento krok nevyhodnotí (je přeskočen)
    printf("%d\n", i);
}
```

---
### default
Součást příkazu `switch`, definuje defaultní hodnotu.

---
### do
Součást cyklu `do while`.

```c
do <příkaz> while (<výraz>)
```
Kód pro výpis druhých mocnin jednociferných čísel s použitím tohoto cyklu by mohl vypadat například takto:
```c
int i = 1;
do {
    printf("%d ", i * i);
    i++;
} while (i < 10);
```

Rozdíl `while` a `do while` 
> `while`: podmínka se testuje před prvním vykonáním těla cyklu.  
> `do-while`: podmínka se testuje po prvním vykonání těla cyklu.

---
### double
Datový typ pro uložení racionálních čísel. Název vznikl historicky, `double` je zkratka z dvojnásobné přesnosti.

---
### else
Příkaz používán s `if`. Více u `if`.

---
### enum
Definuje množinu konstant typu int. Syntaxe je
```c
enum [tag] {name [=value], ...};
```
Například tedy `enum Mocniny {Nulta = 1, Prvni, Druha = 4, Treti = 8};`  

Pokud není uvedena hodnota, pak je hodnota vypočtena jako předchozí hodnota zvětšená o jedničku, pokud chybí hodnota u prvního prvku, je nastavena na 0. Pojmenování množiny není povinné, pokud jej uvedete, můžete je využít při deklaraci proměnných (zde `x` a `y`). `enum Mocniny x, y;`

---
### extern
Klíčové slovo `extern` určuje, že proměnná je uložena a inicializována jinde, či definice funkce (její tělo) je jinde, nejčastěji v jiném modulu.
```c
extern data-definition;
extern function-prototype;
```
```c
extern int celkovyPocet;
extern void faktorial (int n);
```
Prototyp funkce je implicitně `extern`, použití tohoto klíčového slova je tedy nepovinné.

---
### float
Datový typ pro uložení racionálních čísel.

---
### for
Jeden z cyklů. Zápis je následující:
```c
for (<výraz1>; <výraz2>; <výraz3>) <příkaz>;
```
Součet čísel od 1 do 100 včetně:
```c
int sum = 0;
for (int i = 1; i <= 100; i++) {
    sum += i;
};
```

---
### goto
`Goto` slouží k bezpodmínečnému předání řízení. Povolené je vyskočení ven z jednoho bloku viditelnosti, např. z bloku for cyklu ven, nepovolené je skočení dovnitř bloku, například z jedné funkce do druhé.
```c
Opakuj:
  ...
  goto Opakuj;
```
> Ve svých programech se snažte příkazu `goto` vyvarovat.

---
### if
Příkaz sloužící k popisu větvení programu. Uvažujme bludiště dle schématu:

<img src="../images/img.png" alt="maze">

Pokusme se popsat směr, kterým bychom měli pokračovat, pokud se budeme nacházet v jednotlivých místech označených čísly `1`, `2`, `3` a `4`. Text by mohl být zhruba takovýto: ***pokud*** jsi na pozici `1`, ***pak*** jdi přímo dolů, ***pokud*** jsi na pozici `2`, ***pak*** jdi horní cestou vlevo až na konec chodby, pak nahoru a vpravo a opět nahoru a vpravo, ...

V programovací jazyce jako je C se použije příkaz if, který má dvě formy:
```c
if (podmínka) příkaz;

if (podmínka) příkaz;
else příkaz;
```
Pro větší přehlednost (a vyvarování se chyb) se doporučuje důsledně používat blok místo příkazu:

```c
if (podmínka) {
	blok
}

if (podmínka) {
	blok
} else {
	blok
}
```

> Blok může obsahovat deklarace proměnných platných v tomto bloku a dále posloupnost příkazů.

Příklad kódu, který zjistí zda vyšší z čísel `a` a `b` je sudé:
```c
int a = 7, b = 11, max;

if (a > b) {
	max = a;
} else {
	max = b;
}

if (max % 2 == 0) {
	printf("vetsi cislo je sude");
} else {
    printf("vetsi cislo je liche");
}
```
Vlastní postup je rozdělen do dvou kroků, prvním je vybrání větší hodnoty a uložení do proměnné max a druhým krokem je zjištění sudosti a výpis příslušné hlášky.

---
### int 
Datový typ umožňující uchovávat hodnoty typu celé číslo.

---
### long
Datový typ umožňující uchovávat hodnoty typu celé číslo.

---
### register
Klíčové slovo register oznamuje překladači doporučení, aby takto uvozenou proměnnou uložil do registru procesoru a tak urychlil práci s ní. Použití:
```c
register int r;
```

---
### return
Ukončuje běh funkce a výsledek předává volající funkci. Například:
```c
int min (int x, int y) {
	if(x < y) {
		return x;
	}
    // Pokud x < y tak toto neproběhne, funkce vratila hodnotu x 
	return y;
}

int main () {
	int a, b, lower;
	printf("Zadej dve cisla:");
	if (scanf("%d%d",&a, &b) < 2) { /* KONTROLA VSTUPU */
		printf("Nespravny vstup.\n");
		return 1;
	}
	lower = min(a,b); /* VOLANI FUNKCE */
	printf("Mensi je cislo %d\n", lower);
	return 0;
}
```

> Na řádku označeném KONTROLA VSTUPU se kontroluje počet úspěšně načtených položek. Příkaz scanf očekává dvě hodnoty typu int, pokud se je nepodaří načíst, vrátí hodnotu:  
> * 1 - načetl správně pouze jednu,
> * 0 - již první položka se nepodařila,
> * -1 - na vstupu nic nebylo.

1. Funkce `min` má dva parametry, oba jsou typu `int`.
2. První z nich je pro výpočet uvnitř funkce označen `x`, druhý `y`.
3. Jestliže je `x < y`, pak funkce vrátí jako výsledek `x` - je použit `return`.
4. Ihned po `return` se ukončuje běh funkce a další příkazy se neprovedou (není třeba uvádět `else`).
5. Pokud nebyla splněna podmínka (a tedy `x >= y`), pokračuje se za příkazem `if` a vrátí se `y`.
6. Řádek označený `/* VOLANI FUNKCE */` představuje volání funkce. Nejprve jsou vyhodnoceny všechny parametry, v našem případě hodnoty uložené v proměnných `a` a `b`. Hodnoty parametrů jsou uloženy na zásobník, dále se tam ukládá návratová adresa a alokuje místo pro návratovou hodnotu. Pokud funkce obsahuje nějaké lokální proměnné, také se ukládají na zásobník (V našem případě jsou ve funkci `main` lokální proměnné `a`, `b`, `lower`. Funkce `min` nemá žádné).

---
### short
Modifikátor pro datový typ int pro uložení nepříliš velkých celých čísel.

---
### signed
Modifikátor pro datové typy pro ukládání celých čísel. Implicitně jsou tyto datové typy `signed`, použití tohoto klíčového slova je dobrovolné.

---
### sizeof

Operátor `sizeof` se používá pro výpočet velikosti paměti alokované pro datový typ či výraz. Například na platformě, která `int` ukládá na `4` bajty a `double` na `8`, příkazy:
```c
printf(" %zd, ", sizeof(int));
printf(" %zd, ", sizeof 12);
printf(" %zd, ", sizeof 4.4);
printf(" %f, ", sizeof 4.4 - 4.4);
printf(" %zd.\n", sizeof (4.4 - 45));
```
vypíší postupně `4, 4, 8, 3.600000, 8`.

> Unární operátor sizeof má vyšší prioritu než odčítání.

> Operátor `sizeof` vrací hodnotu datového typu `size_t` (dle platformy 32/64 bitové celé číslo). Výstupní formát `%zd` je v C99 preferovaný způsob, jak zobrazovat hodnoty typu `size_t`. Formát `%d` samotný nemusí správně fungovat, pokud je `size_t` velikost 64 bitů, `int` má velikost 32 bitů a používá se big-endian.

---
### static

Syntaxe zápisu je následující:

```c
static definiceDat;
static definiceFunkce;

/* PŘÍKLAD */

static int i = 256;
static void printX (void) {
	putc ('X');
}
```
Klíčové slovo static označuje omezení viditelnosti (viditelnost = rozsah platnosti) proměnné či funkce na právě kompilovanou jednotku. Pokud je klíčové slovo použito u lokální proměnné, nemění její viditelnost, ale specifikuje, že se má uchovávat její hodnota mezi jednotlivými voláními funkce.

Následující kód ukazuje použití klíčového slova u lokální proměnné.

```c
#include <stdio.h>
#include <stdlib.h>

void printX (void) {
	int x = 0;
	x = x + 1;
	printf("%d\n", x);
}

void printStaticX (void) {
	static int x = 0;
	x = x+1;
	printf("%d\n", x);
}

int main () {
	printX();
	printX();
	printX();
	printStaticX();
	printStaticX();
	printStaticX();
	return 0;
}
```
Program vypíše

```c
1
1
1
1
2
3
```

---
### struct
Klíčové slovo `struct` umožňuje seskupovat proměnné do složitější struktury. Vytvořená struktura definuje názvy a typy položek oddělené středníkem, následující příklad ukazuje definici struktury, definici jedné proměnné, její inicializaci v definici a přístup k jednotlivým položkám. Přiřazení jedné proměnné typu struktura druhé nekopíruje pouze reference, ale celý paměťový prostor obsazený strukturou

```c
#include <stdio.h>
#include <stdlib.h>

struct student{
	char jmeno[20];
	double studijniPrumer;
};

int main() {
	struct student chuck = {"Chuck Norris", 0.99};
	printf("Nejlepsi student je %s, jeho studijní prumer je %lf\n", chuck.jmeno, chuck.studijniPrumer);
	return 0;
}
```

---
### switch

Tento příkaz se používá pro větvení programu podle hodnoty uvedené v kulatých závorkách za klíčovým slovem `switch` - očekává se datový typ `int`.
```c
#include <stdio.h>
#include <stdlib.h>

int main(void) {
	int i = 3; /* možno změnit :-) */
	switch (i) {
		case 0:
			printf("\n");
			break;
		case 1:
			printf("+\n");
			break;
		case 2:
			printf("++\n");
			break;
		case 3:
			printf("+++\n");
			break;
		case 4:
			printf("++++\n");
			break;
		default:
			printf("+++++\n");
	}
	return 0;
}
```
Předchozí příklad v závislosti na hodnotě proměnné `i` vypíše na samostatný řádek, `i` krát znak `+`, pokud `i` je `0`, `1`, `2`, `3`, `4`. Pro všechny ostatní hodnoty vypíše znak `+` pětkrát.

> ***Pozor!!!*** Lze napsat `switch` bez `break` v bloku `case`.  
> Program v tomto případě neskončí ale bude pokračovat dále př:
```c
for(int i = 0; i <3; i++){
    switch (i) {
            case 0:
                printf("0\n");
            case 1:
                printf("1\n");
                break;
            case 2:
                printf("2\n");
                break;
	}
}
```
Výstup:
```c
0   // case 0
1   // case 0
1   // case 1
2   // case 2
```

---
### typedef
Klíčové slovo typedef umožňuje definovat vlastní datový typ a zvýšit tak přehlednost kódu.
```c
typedef unsigned char bajt;
typedef char string20[21];
typedef int * ukazatelNaInt;
typedef struct complex {double re, im;} Complex;
typedef int (*ukNaFci)(int);
```
Po uvedení definice můžete nové typy začít používat:
```c
bajt b;
string20 jmeno, prijmeni;
ukazatelNaInt pi;
Complex z;
ukNaFci getFaktorial;
```
se stejným významem jako
```c
unsigned char b;
char jmeno[21], prijmeni[21];
int * pi;
struct complex z; /* nebo též struct {double re, im;} z; */
int (*getFaktorial)(int);
```

--- 
### union
Umožňuje seskupit proměnné s tím, že budou sdílet společný prostor. Zatímco při použití klíčového slova `struct` je alokován dostatek místa, aby bylo možné uložit všechny položky, `union` alokuje místo odpovídající největší položky - položky sdílejí stejný prostor. Pomocí tohoto překrytí lze docílit úsporu místa, například bychom si chtěli vytvořit datový typ pro automobil, `struct` by bylo použito pro SPZ, rok výroby, VIN, průměrnou spotřebu atd. a `union` pro počet osob a nosnost, pro osobní vozidla bychom vyplňovali počet osob, pro nákladní zase nosnost. Nepředpokládá se, že by osobní auta měla nosnost a že by nás zajímal počet přepravovaných osob u nákladních automobilů.
```c
union osobNeboNosnost {
	int osob;
	double nosnost;
} dodatek;
```

Překladač alokuje dostatek místa pro uložení proměnné nosnost. Přístup k položkám je stejný jako u struktur, například `dodatek.nosnost = 56.45;` Obě položky `dodatek.osob` i `dodatek.nosnost` v paměti sdílí stejný prostor. Tohoto faktu je možné využít při zkoumání způsobu uložení hodnot různých datových typů, například při zjišťování endianity.

---
### unsigned
Modifikátor určující, že daný celočíselný typ či char má uchovávat pouze nezáporné hodnoty. Více v kapitole o datovém typu celé číslo.

---
### void
Klíčové slovo void se používá ve třech významech.

1. **Žádný návratový typ**  
    U každé funkce v jazyku C specifikujeme typ hodnoty, kterou funkce vrací. Pokud funkce nevrací nic (někdy se taková funkce označuje pojmem procedura), vyznačíme to klíčovým slovem `void`.


2. **Žádný parametr funkce**  
   Pokud nějaká funkce nemá parametry, vyznačíme to klíčovým slovem `void`. Funkce `printLogo` v ukázce nemá žádný parametr a nic nevrací.
    ```c
    void printLogo (void) {
        printf("%c%c%c\n", 70, 73, 84);
    }
    ```
    > Zápisy funkce `x`: `int x()` a `int x (void)` nemají stejný význam. Zatímco ve druhém případě překladač kontroluje, zda je funkce volána bez parametrů, v prvním případě tak nečiní a neupozorní na špatné volání ani varováním.

3. **Použití u ukazatelů**  
   Ukazatel může mít typ void. Protože by však překladač nevěděl jak velkou paměť dereferencuje, musí být před jeho použitm pro dereferenci explicitně přetypován.
    ```c
    int i;
    double d;
    void *pv = &i;    /* pv ukazuje na i */
    /* TOTO NELZE: *pv = 22;
    warning: dereferencing 'void *' pointer [enabled by default],
    error: invalid use of void expression */
    *(int*)pv = 22; /* v pořádku */
    pv = &d;        /* v pořádku */
    *(double*)pv = 1.1; /* v pořádku */
    ```

---
### volatile
Toto klíčové slovo se používá u definice proměnných k určení, že se s touto proměnnou nemají provádět optimalizace, například přenos do registrů procesoru pro urychlení výpočtu. Důvodem je možnost změny její hodnoty na pozadí, tedy mimo překládaný program. Příkladem je vícevláknová aplikace, nebo aplikace, která používá sdílenou paměť, nebo aplikace na platformě umožňující mapování vstupně-výstupních registrů do adresovatelné paměti, nebo aplikace s hardwarovým přímým přístupem do paměti (toto využívají řadiče disků, grafické a zvukové karty, apod.).
```c
volatile int a;
```
Každé použití takto definované proměnné si vynucuje opětovné načtení z paměti místo možnosti využití rychlého přístupu k případné kopii v registru.

---
### while
Součást cyklu `do while` a cyklu `while`.
```c
do příkaz while (výraz);
while (výraz) příkaz;
```

Kódy pro výpis druhých mocnin jednociferných čísel s použitím cyklu `do while` a `while` by mohly vypadat například takto:
```c
int i = 1;
do {
	printf("%d ", i * i);
	i++;
} while (i < 10);
```
```c
int i = 1;
while (i < 10) {
    printf("%d ", i * i);
    i++;
}
```
`while` cyklus kontroluje splnění podmínky pro první použití a následné opakování vždy před provedením iterace, příkaz `do while` toto provádí po provedení iterace, proto se někdy zjednodušeně uvádí, že tělo `do while` cyklu se provede alespoň jednou (což nemusí být pravda - `break`, `continue`).
