# Keywords

## 2. Keywords

There are 32 keywords in C:

| auto   | break  | case     | char   | const    | continue | default  | do    |
|--------|--------|----------|--------|----------|-----------|---------|-------|
| double | else   | enum     | extern | float    | for      | goto     | if    |
| int    | long   | register | return | short    | signed   | sizeof   | static|
| struct | switch | typedef  | union  | unsigned | void     | volatile | while |

---
### auto
This keyword defines that the following variable has local validity. Notation:
```c
auto int au;
```
Because local validity is preferred for local variables, the auto keyword is very rarely used.

The auto keyword has a different meaning from the C++11 standard, where it denotes a variable whose type is derived by type inference. Since auto is the default memory class for local variables, it is not a good idea to include the keyword

----
### break

Terminates execution of `while`, `do while`, `for`, and `switch` statements. The program then continues after these commands. If more than one of these commands is nested, it terminates the innermost one.

---
### case

Part of the `switch` command

---
### char
Data type for storing characters

---

### const
The `const` keyword is a qualifier indicating that the following variable or pointer used as a parameter cannot be changed. Use with variables `const int max = 20;` defines a constant variable `max` of type `int` and sets it to `20`. Attempting to change it, for example `max = 45;` will cause a compilation error.

> A variable declared with the `const` qualifier can be changed indirectly using the `(int)&max = 35;` pointer.

> The use of define `#define MAX 20` has a similar meaning.  
While the use of const allocates space to store a constant variable, define instructs the preprocessor to replace all occurrences of the `MAX` identifier in the source code with the string `20`. Nothing needs to be allocated.

---

### continue
Terminates execution of the current `while`, `do while`, and `for` statement loop. If more than one of these commands is nested, terminates the innermost one.  
That is, when the code run reaches `continue`, it aborts the code and continues to the next iteration.

Code with `break` will print only `1`
```c
for (int i = 1; i < 10; i++) {
    if (i % 2 == 0)
       break;
    printf("%d\n", i);
}
```  
The following code with `continue` will print only the numbers `1 3 5 7 9`

```c
for (int i = 1; i < 10; i++) {
    if (i % 2 == 0)
        continue;
    // If it is even, this step is not evaluated (it is skipped)
    printf("%d\n", i);
}
```

---
### default
Part of the `switch` command, defines the default value.

---
### to
Part of the `do while` loop.

```c
do <command> while (<expression>)
```
The code for listing the squares of single-digit numbers using this loop might look like this, for example:
```c
int i = 1;
do {
    printf("%d ", i * i);
    i++;
} while (i < 10);
```

The difference between `while` and `do while`
> `while`: the condition is tested before the first execution of the loop body.  
> `do-while`: the condition is tested after the first execution of the loop body.

---
### double
Data type for storing rational numbers. The name originated historically, `double` is an abbreviation of double precision.

---
### else
A command used with `if`. See `if` for more.

---
### enum
Defines a set of constants of type int. The syntax is
```c
enum [tag] {name [=value], ...};
```
So, for example, `enum Powers {Null = 1, First, Second = 4, Third = 8};`

If no value is given, then the value is calculated as the previous value increased by one; if the value is missing for the first element, it is set to 0. Set naming is optional, if you provide it, you can use it when declaring variables (here `x` and `y`). `enum Powers x, y;`

---
### extern
The `extern` keyword indicates that the variable is stored and initialized elsewhere, or the function definition (its body) is elsewhere, most often in another module.
```c
extern data-definition;
extern function-prototype;
```
```c
extern int totalPocet;
extern void factorial (int n);
```
The function prototype is implicitly `extern`, so the use of this keyword is optional.

---
### float
A data type for storing rational numbers.

---
### for
One of the loops. The notation is as follows:
```c
for (<expression1>; <expression2>; <expression3>) <command>;
```
The sum of numbers from 1 to 100 inclusive:
```c
int sum = 0;
for (int i = 1; i <= 100; i++) {
    sum += i;
};
```

---
### goto
`Goto` is used to pass control unconditionally. It is allowed to jump out of a visibility block, e.g., out of a for loop block; it is not allowed to jump inside a block, e.g., from one function to another.
```c
Repeat:
  ...
  goto Repeat;
```
> Try to avoid the `goto` command in your programs.

---
### if
A command used to describe program branching. Consider a maze according to the following diagram:

<img src="../images/img.png" alt="maze">

Let's try to describe the direction we should proceed if we are in the various places marked with `1`, `2`, `3` and `4`. The text might be something like this: ***if*** you are at position `1`, ***then*** go straight down, ***if*** you are at position `2`, ***then*** take the top left path to the end of the corridor, then up and right and up and right again, ...

In a programming language like C, you use the if statement, which has two forms:
```c
if (condition) statement;

if (condition) statement;
else statement;
```
For clarity (and to avoid errors), it is recommended to consistently use a block instead of a command:

```c
if (condition) {
    block
}

if (condition) {
    block
} else {
    block
}
```

> A block can contain variable declarations valid in that block, as well as a sequence of statements.

Example of code that determines whether the higher of `a` and `b` is even:
```c
int a = 7, b = 11, max;

if (a > b) {
    max = a;
} else {
    max = b;
}

if (max % 2 == 0) {
    printf("a larger number is a barrel");
} else {
    printf("the larger number is odd");
}
```
The actual procedure is divided into two steps, the first is to select the larger value and store it in the max variable, and the second step is to determine evenness and print the appropriate message.

---
### int
A data type that allows values of the integer type to be stored.

---
### long
Data type allowing to store values of the integer type.

---
### register
The register keyword tells the compiler to store the variable so quoted in a processor register to speed up its handling. Usage:
```c
register int r;
```

---
### return
Terminates the function and passes the result to the calling function. For example:
```c
int min (int x, int y) {
    if(x < y) {
       return x;
    }
    // If x < y this does not happen, the function returns the value of x 
    return y;
}

int main () {
    int a, b, lower;
    printf("Enter two numbers:");
    if (scanf("%d%d",&a, &b) < 2) { /* CHECK INPUT */
       printf("Incorrect input.\n");
       return 1;
    }
    lower = min(a,b); /* CALL FUNCTIONS */
    printf("Minor is the number %d\n", lower);
    return 0;
}
```

> On the line labeled INPUT CONTROL, the number of items successfully loaded is checked. The scanf command expects two values of type int, if it fails to load them, it returns the value:
> * 1 - only one was read correctly,
> * 0 - the first item has already failed,
> * -1 - there was nothing in the input.

1. The `min` function has two parameters, both of type `int`.
2. The first one is labeled `x` for calculation inside the function, the second one is labeled `y`.
3. If `x < y`, then the function returns `x` as the result - `return` is used.
4. Immediately after `return`, the function is terminated and no further statements are executed (no need to specify `else`).
5. If the condition is not satisfied (and thus `x >= y`), the `if` statement is continued and `y` is returned.
6. The line labeled `/* FUNCTION CALLS */` represents a function call. First all parameters are evaluated, in this case the values stored in the variables `a` and `b`. The parameter values are stored on the stack, then the return address is stored there and space is allocated for the return value. If the function contains any local variables, they are also stored on the stack (In our case, the local variables `a`, `b`, `lower` are in the `main` function. The `min` function has none).

---
### short
Modifier for the int data type to store not very large integers.

---
### signed
Modifier for data types for storing integers. By default these data types are `signed`, the use of this keyword is optional.

---
### sizeof

The `sizeof` operator is used to calculate the size of memory allocated for a data type or expression. For example, on a platform that stores `int` in `4` bytes and `double` in `8` bytes, statements:
```c
printf(" %zd, ", sizeof(int));
printf(" %zd, ", sizeof 12);
printf(" %zd, ", sizeof 4.4);
printf(" %f, ", sizeof 4.4 - 4.4);
printf(" %zd.\n", sizeof (4.4 - 45));
```
prints `4, 4, 8, 3.600000, 8` sequentially.

> The unary operator sizeof has higher priority than subtraction.

> The `sizeof` operator returns the value of the `size_t` data type (a 32/64 bit integer according to the platform). The output format `%zd` is the preferred way to display values of type `size_t` in C99. The `%d` format itself may not work properly if `size_t` is 64 bits, `int` is 32 bits, and big-endian is used.

---
### static

The notation syntax is as follows:

```c
static definitionDat;
static definitionFunction;

/* EXAMPLE */

static int i = 256;
static void printX (void) {
    putc('X');
}
```
The static keyword denotes the restriction of the visibility (visibility = validity range) of a variable or function to the currently compiled unit. When the keyword is applied to a local variable, it does not change its visibility, but specifies that its value is to be stored between function calls.

The following code shows the use of the keyword on a local variable.

```c
#include <stdio.h>
#include <stdlib.h>

void printX(void) {
    int x = 0;
    x = x + 1;
    printf("%d\n", x);
}

void printStaticX(void) {
    static int x = 0;
    x = x+1;
    printf("%d\n", x);
}

int main() {
    printX();
    printX();
    printX();
    printStaticX();
    printStaticX();
    printStaticX();
    return 0;
}
```
The program prints

```c
1
1
1
1
2
3
```

---
### struct
The `struct` keyword allows you to group variables into more complex structures. The created structure defines names and types of items separated by semicolons; the following example shows the definition of a structure, the definition of a single variable, its initialization in the definition, and access to each item. Assigning one variable type to a structure does not copy only the reference, but the entire memory space occupied by the structure

```c
#include <stdio.h>
#include <stdlib.h>

struct student{
    char name[20];
    double studentPrumer;
};

int main() {
    struct student chuck = {"Chuck Norris", 0.99};
    printf("The best student is %s, his study average is %lf\n", chuck.nameo, chuck.studyPrumer);
    return 0;
}
```

---
### switch

This command is used to branch the program according to the value in round brackets after the `switch` keyword - the `int` data type is expected.
```c
#include <stdio.h>
#include <stdlib.h>

int main(void) {
    int i = 3; /* can be changed :-) */
    switch (i) {
       case 0:
          printf("\n");
          break;
       case 1:
          printf("+\n");
          break;
       case 2:
          printf("++\n");
          break;
       case 3:
          printf("+++\n");
          break;
       case 4:
          printf("++++\n");
          break;
       default:
          printf("+++++\n");
    }
    return 0;
}
```
The previous example, depending on the value of the variable `i`, prints on a separate line, `i` times the character `+` if `i` is `0`, `1`, `2`, `3`, `4`. For all other values, it prints the `+` character five times.

> ***Warning!!!*** You can write `switch` without `break` in the `case` block.  
> The program will not terminate in this case but will continue, e.g:
```c
for(int i = 0; i < 3; i++){
    switch (i) {
            case 0:
                printf("0\n");
            case 1:
                printf("1\n");
                break;
            case 2:
                printf("2\n");
                break;
    }
}
```
Output:
```c
0 // case 0
1 // case 0
1 // case 1
2 // case 2
```

---
### typedef
The keyword typedef allows you to define your own data type and thus increase the clarity of the code.
```c
typedef unsigned char byte;
typedef char string20[21];
typedef int * pointerNaInt;
typedef struct complex {double re, im;} Complex;
typedef int (*ukNaFci)(int);
```
Once the definition is given, you can start using the new types:
```c
byte b;
string20 first name, last name;
pointerNaInt pi;
Complex z;
ukNaFci getFactorial;
```
with the same meaning as
```c
unsigned char b;
char jmeno[21], prijmeni[21];
int * pi;
struct complex z; /* or struct {double re, im;} z; */
int (*getFactorial)(int);
```

--- 
### union
Allows you to group variables so that they share a common space. Whereas the `struct` keyword allocates enough space to store all items, `union` allocates space corresponding to the largest item - items share the same space. By using this overlap, space savings can be achieved, for example, if we wanted to create a data type for a car, `struct` would be used for the number plate, year of manufacture, VIN, average consumption etc. and `union` for the number of people and payload, we would fill in the number of people for cars and payload for trucks. It is not assumed that passenger cars have carrying capacity and we are not interested in the number of people carried for trucks.
```c
union passengerCarrying capacity {
    int passenger;
    double carrying capacity;
} appendix;
```

The compiler allocates enough space for storing the variable carrying capacity. Access to the entries is the same as for structures, for example `addend.carrying capacity = 56.45;` Both `addend.person` and `addend.carrying capacity` share the same space in memory. This fact can be used when examining how values of different data types are stored, for example, when determining endianness.

---
### unsigned
A modifier specifying that the given integer type or char should store only non-negative values. See the chapter on the integer data type for more information.

---
### void
The void keyword is used in three senses.

1. **No return type**  
   For each function in the C language, we specify the type of value that the function returns. If the function returns nothing (sometimes referred to as a procedure), we indicate this with the `void` keyword.


2. **No function parameter**  
   If a function has no parameters, we indicate this with the `void` keyword. The `printLogo` function in the example has no parameter and returns nothing.
    ```c
    void printLogo(void) {
        printf("%c%c%c\n", 70, 73, 84);
    }
    ```
   > The `x` function notations: `int x()` and `int x (void)` do not have the same meaning. While in the latter case the compiler checks whether the function is called without parameters, in the former case it does not do so and does not warn about the wrong call even with a warning.

3. **Use with pointers**  
   A pointer can have type void. However, since the compiler would not know how much memory it is dereferencing, it must be explicitly retyped before it is used for dereferencing.
    ```c
    int i;
    double d;
    void *pv = &i; /* pv points to i */
    /* THIS CANNOT: *pv = 22;
    warning: dereferencing 'void *' pointer [enabled by default],
    error: invalid use of void expression */
    *(int*)pv = 22; /* ok */
    pv = &d; /* ok */
    *(double*)pv = 1.1; /* okay */
    ```

---
### volatile
This keyword is used in variable definitions to specify that no optimizations, such as transferring to processor registers to speed up computation, are to be performed on this variable. This is because its value can be changed in the background, outside the program being compiled. An example is a multi-threaded application, or an application that uses shared memory, or an application on a platform that allows mapping I/O registers to addressable memory, or an application with hardware direct memory access (this is used by disk controllers, graphics and sound cards, etc.).
```c
volatile int a;
```
Each use of a variable defined in this way forces a reload from memory instead of being able to use a quick access to a possible copy in the registry.

---
### while
Part of the `do while` loop and the `while` loop.
```c
do while statement (expression);
while (expression) statement;
```

Codes for listing the squares of single-digit numbers using the `do while` and `while` loops might look like this, for example:
```c
int i = 1;
do {
    printf("%d ", i * i);
    i++;
} while (i < 10);
```
```c
int i = 1;
while (i < 10) {
    printf("%d ", i * i);
    i++;
}
```
The `while` loop always checks for the first use and subsequent iteration before the iteration is executed, the `do while` statement does this after the iteration is executed, so it is sometimes simplistically stated that the body of the `do while` loop is executed at least once (which may not be true - `break`, `continue`).
