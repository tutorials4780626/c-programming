# Pointers

## 1. Základní použití

Deklarace proměnné typu ukazatel:

```c
int * pi;
char * pc;
```

Proměnná `pi` je ukazatel na `int`, `pc` je ukazatel na `char`. Typ `int`, popř. `char` je **doménový typ ukazatele**.
Ukazateli lze přiřadit adresu nějaké proměnné:

```c
int * pi, i;
char c;
char * pc = &c;
pi = &i;
```

Unární prefixový operátor `&` označuje operaci **reference**. Chceme-li použít proměnnou, na kterou ukazuje ukazatel,
vyjádříme to pomocí unárního operátoru **dereference**. Tedy operátor `*`

| Kód v C      | Vysvětlení činnosti                                                                  |
|--------------|--------------------------------------------------------------------------------------|
| `char c;`    | Deklarace proměnné `c` typu `char`.                                                  |
| `char * pc;` | Deklarace proměnné `pc`, její typ je ukazatel na `char`.                             |
| `pc = &c;`   | Navázání proměnné `pc` na `c`. V `pc` je nyní adresa proměnné `c` v paměti programu. |
| `*pc = 'A';` | Na adresu `pc` je uložena hodnota odpovídající znaku `'A'`.                          |

---

### NULL

Jako „prázdný, neplatný“ ukazatel, který neukazuje na žádnou proměnnou, slouží v jazyku C hodnota `0`. Symbolickým
označením tohoto ukazatele je `NULL`. Dereference prázdného ukazatele způsobí chybu při běhu programu:

```c
int main(void) {
	int * p = NULL;
	*p = 10; /* pokus o dereferenci neplatného ukazatele */
	return 0;
}
```

---

### Void*

Pokud potřebujeme vytvořit ukazatel bez doménového typu (třeba z důvodu, že nevíme v době návrhu, jaký tam nakonec bude)
použijeme typ `void *`. Tomuto ukazateli můžeme přiřadit hodnotu, ale nemůžeme jej dereferencovat, což je kontrolováno
ve fázi překladu programu.

```c
int main(void) {
	int  i;
	void * pi = &i;
	*pi = 10; /* toto nelze */
	return 0;
}
```

---

### Ukazatelová aritmetika

Ukazatele představují adresy do paměti programu, ale nejsou pro ně povoleny všechny aritmetické operace. Předpokládejme,
že máme dva ukazatele, jejichž **společný doménový typ je T** a dále číslo typu `int`.

| Operace    | Typ výsledku | Popis                                                                              |
|------------|--------------|------------------------------------------------------------------------------------|
| `T* + int` | `T*`         | Přičtení `n` k ukazateli typu `T*` znamená jeho posun o n-násobek délky typu `T`.  |
| `T* - int` | `T*`         | Odečtení `n` od ukazatele typu `T*` znamená jeho posun o n-násobek délky typu `T`. |
|            |              | obdobně fungují prefixové i postfixové operátory `++ a --`.                        |
| `T* - T*`  | `int`        | Počet prvků typu `T`, které lze uložit mezi adresy specifikované operandy.         |
| `T* < T*`  | `int`        | Zjistí, zda první ukazatel ukazuje v paměti před druhý.                            |
|            |              | Obdobně pro další relační operátory `>`, `<=`, `>=`, `==`, `!=`.                   |
|            |              | Zde není požadován stejný doménový typ ukazatelů.                                  |

### Použití nesprávného ukakazatele

Jazyk C nijakým způsobem neeviduje jaký typ informace se na jednotlivých místech v paměti vyskytuje, tedy pokud
přistupujeme do paměti, musíme jako programátoři vědět, jaký typ dat se na daném místě nachází. Programu tuto informaci
předáváme pomocí doménového typu ukazatele, který používáme. Použití nesprávného ukazatele vede ke špatně odhalitelné
chybě:

```c
int main (void) {
	int * pi;  /* !!!!! ukazatel na int!!!! */
	char c[] = {'A','h','o','j', ' ', 'F', 'I', 'T', 'e', '\0'};
	printf("obsah c: %s\n",c); /* kontrola řetězce */
	pi = &c[5];                /* nastav ukazatel na šestý znak, na F */
	printf("adresa v c: %u\n", c);
	printf("adresa c[5]: %u\n", &c[5]);
	printf("adresa v pi: %u\n", pi);
	printf("obsah c: %s\n", c);
	*pi = 'B';  /* přepiš F na B?????????????? */
	printf("obsah c: %s\n", c);
	return 0;
}
```

Výstup programu může vypadat například takto (výsledek záleží na endianitě, aktuálním obsazení paměti, ..):

```c
obsah c: Ahoj FITe
adresa v c: 2293554
adresa c[5]: 2293559
adresa v pi: 2293559
obsah c: Ahoj FITe
obsah c: Ahoj B
```

Z předchozího kódu je vidět, že při použití ukazatele na int (nejčastěji 4 bajty) pracuje nejen s jedním bajtem, kde se
nachází znak `F`, ale i se 3 sousedy

---

## 2. Ukazatel pole

Pole typu `T` je v podstatě konstantní ukazatel na T: Předpokládejme `int a[10], *pa, i;`, pak

| Výraz                                   | Ekvivalentní výraz                   | Komentář                                              |
|-----------------------------------------|--------------------------------------|-------------------------------------------------------|
| `pa = a`                                | `pa = &a[0]`                         | Pole je vlastně adresa prvního prvku.                 |
| `*(a + 2) = 3`                          | `a[2] = 3`                           | Třetímu prvku nastaví hodnotu 3.                      |
| `*(2 + a) = 3`                          | `2[a] = 3`                           | Třetímu prvku nastaví hodnotu 3, nehezké být funkční. |
| `*(a + i) = 4`                          | `a[i] = 4`                           | Zobecnění.                                            |
| `for (pa = a; pa < a + 10; *pa++ = 0);` | `for (i = 0; i < 10; i++) a[i] = 0;` | Pole lze procházet pomocí ukazatele.                  |
| `int sectiPrvky(int pole[])`            | `int sectiPrvky(int * pole)`         | Pole jako parametr funkce.                            |

### Kombinace * a ++

| Výraz    | Význam                                                                                   |
|----------|------------------------------------------------------------------------------------------|
| `*p++`   | Přistup k datům odkazovaným ukazatelem p a poté inkrementuj p (posuň jej na další prvek) |
| `(*p)++` | Přistup k datům odkazovaným ukazatelem p a poté inkrementuj tato data.                   |
| `*++p`   | Nejprve inkrementuj p a pak přistup k datům odkazovaným ukazatelem p.                    |
| `++*p`   | Nejprve inkrementuj data odkazovaná ukazatelem p a pak tato data použij.                 |

### Užití const

Pokud deklarujeme pole, pak proměnná, která jej reprezentuje se nesmí měnit (kontrolováno na úrovni překladu). Tedy
nelze:

```c
int a[10];
a++;
```

| Deklarace funkce               | Význam                                                                                     |
|--------------------------------|--------------------------------------------------------------------------------------------|
| `int xyz(const int * uk);`     | Nesmí se měnit data, na která ukazuje **uk**, ale **uk** je možné přesměrovat na jiná data |
| `int xyz(int * const uk);`     | Nesmí se přesměrovat ukazatel **uk**, ale jím referovaná data lze měnit.                   |
| `int xyz(const int * const uk` | Nesmí se měnit **uk** ani data.                                                            |

### Přiřazení mezi poli

Pro pole není definováno přiřazení. Tedy představa, že lze do proměnné typu pole přenést jednoduše data z jiného pole je
lichá:

```c
int a[2], b[] = {1,2};
a = b;
```

Toto nelze. Pokud bychom potřebovali přenést data z jednoho pole do druhého, musíme to udělat cyklem prvek po prvku nebo
můžeme využít funkce, které mohou pracovat s většími úseky paměti a kopírování tak urychlit. Jednou z těchto funkcí je
funkce [memcpy](https://en.cppreference.com/w/c/string/byte/memcpy):

```c
void * memcpy(void * dest, const void * src, size_t count);
```

Naproti tomu přiřazení mezi ukazateli definované je, opět je nutné si uvědomit, že se nekopírují referovaná data, pouze
se přesměruje ukazatel:

```c
int main(void) {
	int * p = (int *)malloc(2*sizeof(int)); // Dynamicky alokujeme pole o dvou prvcích.
	int * q = (int *)malloc(2*sizeof(int)); // A ještě jedno.
	p[0] = 1; // Hodnotu prvního prvku pole p nastavíme na 1.
	q[0] = 22; // Obdobně pro první prvek druhého pole.
	p = q; // Přesměrováváme ukazatel, nekopírují se hodnoty a blok paměti alokovaný pro p
	       // se stává nedosažitelným a operační systém jej uvolní až program skončí.
	printf("%d\n", p[0]); // Vypíše 22, protože p ukazuje na totéž místo jako q.
	q[0] = 42; // Nastaví první prvek pole q.
	printf("%d\n", p[0]); // Vypíše 42, oba ukazatele referují stejnou hodnotu!
	free(p); // Uvolní paměť alokovanou pro q.
	free(q); // Paměť pro pole q již byla uvolněna, pokud ji mezitím operační systém
	         // přidělil jiné proměnné, asi to nedopadne dříve či později dobře.
	return 0; // Neuvolnili jsme paměť a zariskovali s free, přesto vracíme úspěch.
}
```

